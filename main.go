package main

import (
	"github.com/gin-gonic/gin"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

type Key struct {
	Name  string      `json:"name"`
	Value interface{} `json:"value"`
	Doc   string      `json:"doc"`
}

type Config struct {
	Name   string   `json:"name"`
	Keys   []Key    `json:"keys"`
	Childs []Config `json:"childs"`
}

func writeConfig(gc *gin.Context, accept string, config *Config) {
	m := map[string]func(code int, obj any){
		"yaml": gc.YAML,
		"toml": gc.TOML,
		"xml":  gc.XML,
	}
	for k, v := range m {
		if strings.Contains(accept, k) {
			v(http.StatusOK, &config)
			return
		}
	}
	gc.JSON(http.StatusOK, config)
}

func getSubConfig(path []string) Config {
	var c Config = Config{Childs: db.read()}
	for _, p := range path {
		if p != "" {
			for _, x := range c.Childs {
				if x.Name == p {
					c = x
					break
				}
			}
		}
	}
	return c
}

func getConfig(gc *gin.Context) {
	path := strings.Split(gc.Param("action"), "/")
	c := getSubConfig(path[1:])
	gc.Header("Access-Control-Allow-Origin", "*")
	writeConfig(gc, gc.GetHeader("Accept"), &c)
}

func addConfig(gc *gin.Context) {
	path := strings.Split(gc.Param("action"), "/")
	c := getSubConfig(path[1:len(path)-1])

	var config Config
	err := gc.Bind(&config)
	if err != nil {
		gc.JSON(http.StatusNotAcceptable, err)
	} else {
		c.Childs = append(c.Childs, config)
		db.write(c.Childs)
		gc.JSON(http.StatusAccepted, config)
	}
}

type IDB interface {
	read() []Config
	write([]Config)
}

type YamlDB struct{}
func (db YamlDB) read() []Config {
	data, err := ioutil.ReadFile("config.yaml")
	if err != nil {
		log.Println(err)
	}
	var config []Config
	err = yaml.Unmarshal(data, &config)
	if err != nil {
		log.Println(err)
	}
	return config
}
func (db YamlDB) write(config []Config) {
	config_byte, _ := yaml.Marshal(config)
	err := ioutil.WriteFile("config.yaml", config_byte, 0777)
	if err != nil {
		log.Println(err)
	}
}

var db IDB

func main() {
	db = YamlDB{}

	router := gin.Default()
	api := router.Group("/api")
	api.GET("/config/*action", getConfig)
	api.POST("/config/*action", addConfig)

	router.Run(":8080")
}
