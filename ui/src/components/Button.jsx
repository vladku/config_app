import './Button.css';

export const Button = ({name, cls, onClick}) => (
    <div class={"button " + cls}
        onClick={(e) => { if (onClick) { onClick() } }}>
        <span>{name}</span>
    </div>
)