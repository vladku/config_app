import './Input.css';
import { createSignal } from "solid-js";

export function Input({value, label, onInput, isDisabled}) {
    const [cvalue, setValue] = createSignal(value ?? "" );
    const [disabled, setDisabled] = createSignal(isDisabled);
    const setInput = (v) => {
        setValue(v);
        if (onInput) {
            onInput(v);
        }
    };
    return <div class="input-line-group">
        <div class="input-group tooltip">      
            <input type="string"
                class={disabled() ? "": "editable"}
                value={cvalue()}
                readonly={disabled()}
                required
                onInput={(e) => setInput(e.currentTarget.value)}
                onFocusOut={isDisabled ? (e) => setTimeout(setDisabled, 200, true) : null}
                onDblClick={(e) => setDisabled(false) }/>
            <label >{label}</label>
            { disabled() ? <span class="tooltiptext color blue">Double click to unlock editing</span> : <></> }
            <i onClick={(e) => { if (!disabled()) { setInput("") } }} 
                className={(disabled() ? "fa-lock" : "fa-times-circle") + " fa red color" }></i>
        </div>
    </div>
}