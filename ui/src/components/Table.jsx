import './Table.css';
import { Input } from './Input'

const KeyRows = (props) => (
  <>
  {props.data && props.data.keys && Object.entries(props.data.keys).map(([_, v]) => (
    <tr>
      <td>{v.name}</td>
      <td className="input"><Input value={v.value.toString()}></Input></td>
      <td>{v.doc}</td>
    </tr>
  ))}
  {props.data && props.data.childs && Object.entries(props.data.childs).map(([k, v]) => (
    <KeyRows data={v} />
  ))}
  </>
)
function get_all_keys(config) {
    var result = []
    if (config.keys) {
        Object.entries(config.keys).map(([_, v]) => result.push(v))
    }
    if (config.childs) {
        Object.entries(config.childs).map(([_, v]) => result = result.concat(get_all_keys(v)))
    }
    return result
}
export const Keys = (props) => (
  <>
    <table className="styled-table">
      <thead>
        <tr>
          <th>Name</th>
          <th>Value</th>
          <th>Type</th>
          <th>Description</th>
        </tr>
      </thead>
      <tbody>
        {props.data && get_all_keys(props.data).map((v) => (
            <tr>
                <td>{v.name}</td>
                <td className="input">
                    <Input value={v.value && v.value.toString()} 
                        isDisabled />
                </td>
                <td>{Array.isArray(v.value) ? "array" : typeof v.value}</td>
                <td>{v.doc}</td>
            </tr>
        ))}
      </tbody>
    </table>
  </>
)