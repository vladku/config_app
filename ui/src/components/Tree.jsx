import './Tree.css';

import { Input } from './Input';
import { createSignal, createResource } from "solid-js";

function filter(array, text) {
    const getNodes = (result, [k, v]) => {
        if ((v.name && v.name.includes(text)) || text === "") {
            result[k] = v;
            return result;
        }
        if (v.childs !== undefined && v.childs !== null) {
            const nodes = Object.entries(v.childs).reduce(getNodes, {});
            var c = {}
            c.name = v.name
            c.childs = nodes
            c.keys = v.childs
            if (Object.keys(nodes).length !== 0) result[k] = c;
        }
        return result;
    };
    return Object.entries(array).reduce(getNodes, {});
}

function Tree(props) {
    const [filterText, setfilterText] = createSignal("");
    const filterConfig = (text) => {
        var aa = {}
        var a = props.data
        if (a !== undefined && a !== null 
                && a.childs !== undefined && a.childs !== null) {
            aa.keys = a.keys
            aa.childs = filter(a.childs, text)
            return aa
        } else { return a }
    }
    const [config] = createResource(filterText, filterConfig);
    if (!props.path) { props.path = ""; }
    var u = <ul>
        {config() && config().childs && Object.entries(config().childs)
            // .filter(([key, _]) => props.filter == "" || key.includes(props.filter))
            .map(([_, v]) => {
                const [active, setActive] = createSignal(true);
                return <li class={active() ? "active" : ""}>
                    <h3><a onClick={(e) => {
                                props.get_config(`${props.path}/${v.name}`)
                                setActive(!active())
                        }}>{v.childs !== null && Object.entries(v.childs).length > 0 
                            ? (active() ? <>&#8595</> : <>&#8594</>) 
                            : ''} {v.name}</a></h3>
                    {<Tree data={v} 
                           child={true}
                           path={`${props.path}/${v.name}`}
                           get_config={props.get_config}/>}
                </li>
            })}
        </ul>
    if (props.child) {return u}
    return <div className="accordian">
        <Input
            label="Filter config"
            onInput={setfilterText}
        />
        <span>{config.loading && "Filtering..."}</span>
        {u}
    </div>
}

export {Tree};