import { createSignal, createResource } from "solid-js";
import { Tree } from './components/Tree';
import { Keys } from "./components/Table";
import { Button } from "./components/Button";

const fetchConfig = async (path) =>
  (await fetch(`http://localhost:8080/api/config/${path}`)).json();

function App() {
  const [configJson, setConfigJson] = createSignal("");
  const [config] = createResource(configJson, fetchConfig);
  return (
    <div className="container">
      {config() 
        ? <Tree data={config()}
                get_config={setConfigJson}/>
        : <span>Config Loading...</span>}
      <div>
        <div style="display: flex">
          <Button name="TEst" 
                cls="color green"
                onClick={(e) => document
                  .getElementsByTagName("html")[0]
                  .classList.toggle("dark")}/>
          <Button name="Save" />
          <Button name="Red" cls="color red"/>
        </div>
        <Keys data={config()} />
      </div>
    </div>
  );
}

export default App;
